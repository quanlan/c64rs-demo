
from Utils.data_preload import load_and_preprocess_data
from Utils.time_frequency_analysis import plot_time_frequency
from Utils.bandpower_analysis import calculate_bandpower
from Utils.psd_analysis import calculate_psd





bdf_file_path=[
               # rf"D:\Data\C64\MedicalTest\Sleep\20240618132757_XGN_Sleep.bdf",
               # rf"D:\Data\C64\MedicalTest\EyeOpen_EyeClose\Close\EyeClose.bdf",
               # rf"D:\Data\C64\MedicalTest\EyeOpen_EyeClose\Open\EyeOpen.bdf",
               # rf"D:\Data\C64\MedicalTest\Monkey\day\20240411210338_78_day.bdf",
               # rf"D:\Data\C64\MedicalTest\Monkey\night\20240411210338_22_night.bdf",
               # rf"D:\Data\C64\MedicalTest\Awake\20240618162217_XGN_Awake.bdf",
               #  rf"D:\Data\C64\TJ_ane_LJH\20240619\Awake\awake.bdf",
                rf"D:\Data\C64ProSpike\20240731\0omh\0ohm.edf",
                rf"D:\Data\C64ProSpike\20240731\1Momh\1Momh.edf",
                rf"D:\Data\C64ProSpike\20240731\10Komh\10kohm.edf",
                # rf"D:\Data\C64\TJ_ane_LJH\EGIdata\0619002QJ_20240619_125231.mff",
               ]




for data_path in bdf_file_path:
    data_dict,result_figure_path=load_and_preprocess_data(data_path,start_sec=0,end_sec=-1,Filter=True)
    plot_time_frequency(data_dict,result_figure_path,interval=3,window = 5)
    calculate_bandpower(data_dict, result_figure_path)
    calculate_psd(data_dict, result_figure_path, fmin=0, fmax=200,freq_res=0.01)

    print()


