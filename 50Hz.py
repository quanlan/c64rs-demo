import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import welch

# 生成参数
fs = 1000  # 采样频率
t = np.arange(0, 1.0, 1.0 / fs)  # 时间轴
f = 200  # 正弦信号频率

# 生成50Hz的正弦信号
signal = np.sin(2 * np.pi * f * t)

# 使用Welch方法计算PSD
frequencies, psd = welch(signal, fs, nperseg=1000)

# 绘图
plt.figure(figsize=(10, 6))
plt.subplot(2, 1, 1)
plt.plot(t, signal)
plt.title('50Hz正弦信号')
plt.xlabel('时间 [秒]')
plt.ylabel('幅值')

plt.subplot(2, 1, 2)
plt.semilogy(frequencies, psd)
plt.title('功率谱密度 (PSD)')
plt.xlabel('频率 [Hz]')
plt.ylabel('PSD [V**2/Hz]')
plt.tight_layout()
plt.show()
