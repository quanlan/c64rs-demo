# eeg_analysis/time_frequency_analysis.py

import os
import numpy as np
import matplotlib.pyplot as plt
from datetime import timedelta
import yasa

def plot_time_frequency(data_dict, result_dir, use_percentile=True,interval = 5,window = 30):

    for channel, channel_data in data_dict['Data'].items():
        plt.subplots(figsize=(19, 4.5))

        f, t, Sxx = yasa.stft_power(channel_data, data_dict['SampleFrequency'], window=window, step=int(0.5 * window), band=(0, 35), interp=False)
        Sxx = 10 * np.log10(Sxx)

        if use_percentile:
            vmin = np.percentile(Sxx, 5)
            vmax = np.percentile(Sxx, 95)

            plt.pcolormesh(t, f, Sxx,
                           shading='gouraud',
                           # cmap='viridis',
                           cmap='jet',
                           vmin=vmin, vmax=vmax)
        plt.ylabel('Frequency (Hz)')
        plt.xlabel('Time (s)')
        plt.title(f'{channel} Time-Frequency')
        plt.ylim([0, 35])
        plt.colorbar(label='Power Spectral Density (dB)')
        plt.tight_layout()

        # 将时间间隔转换为秒
        interval_seconds = interval * 60

        # 计算所有t时间点的标签
        all_tick_labels = [(data_dict['StartTime'] + timedelta(seconds=tick)).strftime('%H:%M:%S')
                           for tick in t]  # 假设data_dict已经定义且包含'StartTime'

        # 筛选显示的刻度标签索引
        display_ticks_indices = np.arange(0, len(t), int(interval_seconds/int(0.5 * window)))
        display_ticks_indices = display_ticks_indices.astype('int16')
        display_ticks_indices = display_ticks_indices[display_ticks_indices < len(t)]  # 确保索引不会超出范围
        # 提取显示的刻度和对应的标签
        display_ticks = t[display_ticks_indices]
        display_tick_labels = [all_tick_labels[i] for i in display_ticks_indices]
        # 设置x轴刻度和标签
        plt.xticks(display_ticks, display_tick_labels, rotation=45)
        plt.savefig(os.path.join(result_dir, f'{channel}_time_frequency.png'))
        plt.close()
