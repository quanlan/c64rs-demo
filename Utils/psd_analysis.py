import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import welch

def calculate_psd(data_dict, result_dir, fmin=0, fmax=50, freq_res=0.01):
    psd_values = {}
    all_psd = []
    paf_values = {}
    paf_psd_values = {}

    for channel, channel_data in data_dict['Data'].items():
        # Compute PSD
        # nperseg = int(data_dict['SampleFrequency'])/ freq_res
        # freqs, psd = welch(channel_data, data_dict['SampleFrequency'], nperseg=nperseg)
        freqs, psd = welch(channel_data, data_dict['SampleFrequency'], nperseg=4096)
        plt.semilogy(freqs, psd)
        # plt.plot(freqs, psd, label='Average PSD', color='black')
        plt.title('功率谱密度 (PSD)')
        plt.xlabel('频率 [Hz]')
        plt.ylabel('PSD [V**2/Hz]')
        plt.tight_layout()
        plt.show()


        # freqs, psd = welch(channel_data, data_dict['SampleFrequency'])
        psd[psd <= 0] = np.nan
        # psd = 10 * np.log10(psd)
        freqs_filtered = freqs[(freqs >= fmin) & (freqs <= fmax)]
        psd_filtered = psd[(freqs >= fmin) & (freqs <= fmax)]
        psd_values[channel] = psd_filtered
        all_psd.append(psd_filtered)

        # Find peak alpha frequency
        alpha_indices = np.where((freqs_filtered >= 8) & (freqs_filtered <= 13))[0]
        if len(alpha_indices) > 0:
            peak_alpha_index = np.argmax(psd_filtered[alpha_indices])
            peak_alpha_freq = freqs_filtered[alpha_indices][peak_alpha_index]
            paf_values[channel] = peak_alpha_freq
            paf_psd_values[channel] = psd_filtered[alpha_indices][peak_alpha_index]
        else:
            paf_values[channel] = np.nan
            paf_psd_values[channel] = np.nan

        # Plot PSD and save figures
        plt.figure()
        plt.plot(freqs_filtered, psd_filtered, color='black')
        plt.title(f'{channel} PSD')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Power Spectral Density (dB/Hz)')
        plt.xlim(min(freqs_filtered), max(freqs_filtered))
        plt.tight_layout()
        plt.savefig(os.path.join(result_dir, f'{channel}_psd.png'))
        plt.close()

    # Calculate average PSD
    avg_psd = np.nanmean(all_psd, axis=0)
    psd_values['Average'] = avg_psd

    # Plot and save average PSD
    plt.figure()
    plt.plot(freqs_filtered, avg_psd, label='Average PSD', color='black')
    plt.title('Average PSD')
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Power Spectral Density (dB/Hz)')
    plt.xlim(min(freqs_filtered), max(freqs_filtered))
    plt.tight_layout()
    plt.savefig(os.path.join(result_dir, 'average_psd.png'))
    plt.close()

    # Save PSD, PAF, and PAF_PSD to Excel
    psd_df = pd.DataFrame(psd_values, index=freqs_filtered)
    psd_df.to_excel(os.path.join(result_dir, 'psd_values.xlsx'))

    # Merge PAF and PAF_PSD into a single DataFrame
    paf_df = pd.DataFrame.from_dict(paf_values, orient='index', columns=['PAF'])
    paf_psd_df = pd.DataFrame.from_dict(paf_psd_values, orient='index', columns=['PAF_PSD'])
    paf_combined_df = pd.concat([paf_df, paf_psd_df], axis=1)

    with pd.ExcelWriter(os.path.join(result_dir, 'psd_values.xlsx'), mode='a', engine='openpyxl') as writer:
        paf_combined_df.to_excel(writer, sheet_name='PAF_and_PAF_PSD')


