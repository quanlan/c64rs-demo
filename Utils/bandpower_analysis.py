# eeg_analysis/bandpower_analysis.py

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import welch
def calculate_bandpower(data_dict, result_dir):
    bands = {'Delta': (0, 4),
             'Theta': (4, 8), 'Alpha': (8, 13),
             'Beta': (13, 30),
             # 'Gamma': (30, 100)
             }
    bandpower_values = []
    avg_bandpower = {band: {'absolute_total': [], 'relative_total': [], 'absolute_per_Hz': [], 'relative_per_Hz': []}
                     for band in bands.keys()}

    for channel, channel_data in data_dict['Data'].items():
        bandpower = {'Channel': channel}

        for band, (fmin, fmax) in bands.items():
            band_freqs, band_psd = welch(channel_data, data_dict['SampleFrequency'], nperseg=3*int(data_dict['SampleFrequency']/0.48828125))
            band_power = np.sum(band_psd[(band_freqs >= fmin) & (band_freqs <= fmax)])
            total_power = np.sum(band_psd)
            band_width = fmax - fmin
            bandpower[f'{band}_absolute_total'] = band_power  # 单位是uV²
            bandpower[f'{band}_relative_total'] = band_power / total_power  # 无单位
            bandpower[f'{band}_absolute_per_Hz'] = band_power / band_width  # 单位是uV²/Hz
            bandpower[f'{band}_relative_per_Hz'] = (band_power / total_power) / band_width  # 无单位

            avg_bandpower[band]['absolute_total'].append(band_power)
            avg_bandpower[band]['relative_total'].append(band_power / total_power)
            avg_bandpower[band]['absolute_per_Hz'].append(band_power / band_width)
            avg_bandpower[band]['relative_per_Hz'].append((band_power / total_power) / band_width)

        bandpower_values.append(bandpower)



        plot_bandpower(bandpower, bands, result_dir, channel)

    avg_bandpower_values = {'Channel': 'Average'}
    for band in bands.keys():
        avg_bandpower_values[f'{band}_absolute_total'] = np.mean(avg_bandpower[band]['absolute_total'])
        avg_bandpower_values[f'{band}_relative_total'] = np.mean(avg_bandpower[band]['relative_total'])
        avg_bandpower_values[f'{band}_absolute_per_Hz'] = np.mean(avg_bandpower[band]['absolute_per_Hz'])
        avg_bandpower_values[f'{band}_relative_per_Hz'] = np.mean(avg_bandpower[band]['relative_per_Hz'])

    bandpower_values.append(avg_bandpower_values)
    bandpower_df = pd.DataFrame(bandpower_values)
    bandpower_df.to_excel(os.path.join(result_dir, 'bandpower_values.xlsx'), index=False)

    plot_avg_bandpower(avg_bandpower_values, bands, result_dir, channel)

def plot_bandpower(bandpower, bands, result_dir, channel):
    labels = list(bands.keys())
    x = np.arange(len(labels))
    width = 0.35

    for metric, unit in zip(['absolute_total', 'relative_total', 'absolute_per_Hz', 'relative_per_Hz'],
                            ['uV²', 'Ratio', 'uV²/Hz', 'Ratio']):
        plt.figure()
        values = [bandpower[f'{band}_{metric}'] for band in labels]
        plt.bar(x, values, width, label=f'{metric.replace("_", " ").title()}',color='black')
        plt.ylabel(f'Bandpower ({unit})')
        plt.title(f'{channel} {metric.replace("_", " ").title()} Bandpower')
        plt.xticks(x, labels)
        plt.tight_layout()
        plt.savefig(os.path.join(result_dir, f'{channel}_{metric}_bandpower.png'))
        plt.close()

def plot_avg_bandpower(avg_bandpower_values, bands, result_dir,channel):
    labels = list(bands.keys())
    x = np.arange(len(labels))
    width = 0.35

    for metric, unit in zip(['absolute_total', 'relative_total', 'absolute_per_Hz', 'relative_per_Hz'],
                            ['uV²', 'Ratio', 'uV²/Hz', 'Ratio']):
        plt.figure()
        values = [avg_bandpower_values[f'{band}_{metric}'] for band in labels]
        plt.bar(x, values, width, label=f'{metric.replace("_", " ").title()}',color='black')
        plt.ylabel(f'Bandpower ({unit})')
        plt.title(f'Average {metric.replace("_", " ").title()} Bandpower({channel})')
        plt.xticks(x, labels)
        plt.tight_layout()
        plt.savefig(os.path.join(result_dir, f'average_{metric}_bandpower.png'))
        plt.close()

