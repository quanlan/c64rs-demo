import os

import matplotlib.pyplot as plt
import mne
from scipy.signal import welch, butter, filtfilt
import numpy as np
from datetime import datetime, timedelta

def load_and_preprocess_data(bdf_file_path,start_sec,end_sec,Filter=True):

    def select_time_range(data, sfreq, start_sec, end_sec):
        """
        提取指定时间范围内的数据。

        参数:
            data (np.array): 通道数据数组。
            sfreq (float): 采样率。
            start_sec (float): 开始时间（秒）。
            end_sec (float): 结束时间（秒）。

        返回:
            np.array: 时间范围内的数据。
        """
        start_idx = int(start_sec * sfreq)
        if end_sec == -1:
            end_idx = -1
        else:
            end_idx = int(end_sec * sfreq)
        return data[:, start_idx:end_idx]



    def notch_filter(EEG, fs, order=3):
        # EEG=EEG*1000*1000*2# convert to uV
        EEG_filtered=np.squeeze(EEG*1000*1000)# convert to uV
        # # EEG_filtered=EEG
        nyquist = 0.5 * fs
        low_cutoff = 49 / nyquist
        high_cutoff = 51 / nyquist
        b, a = butter(order, [low_cutoff, high_cutoff], btype='bandstop')

        EEG_filtered = filtfilt(b, a,EEG_filtered)

        low_cutoff = 99 / nyquist
        high_cutoff = 101 / nyquist
        b, a = butter(order, [low_cutoff, high_cutoff], btype='bandstop')
        EEG_filtered = np.squeeze(filtfilt(b, a,EEG_filtered))

        low_cutoff = 149 / nyquist
        high_cutoff = 151 / nyquist
        b, a = butter(order, [low_cutoff, high_cutoff], btype='bandstop')
        EEG_filtered = np.squeeze(filtfilt(b, a, EEG_filtered ))

        # low_cutoff = 1 / nyquist
        # high_cutoff = 45 / nyquist
        # b, a = butter(order, [low_cutoff, high_cutoff], btype='bandpass')
        # EEG_filtered = np.squeeze(filtfilt(b, a, EEG_filtered ))

        return EEG_filtered

    def read_bdf_and_extract_data(bdf_file_path,start_sec,end_sec):
        # 读取bdf文件

        # raw = mne.io.read_raw_egi(bdf_file_path, preload=True)

        raw = mne.io.read_raw_edf(bdf_file_path, preload=True)

        # 从文件中获取采样率和开始时间
        data_dict_raw = {ch: select_time_range(raw.get_data(picks=ch), raw.info['sfreq'], start_sec, end_sec) for ch in
                         raw.ch_names}

        data_dict ={'Data':data_dict_raw}


        data_dict['SampleFrequency'] = raw.info['sfreq']
        data_dict['StartTime']= raw.info['meas_date'].replace(tzinfo=None)+ timedelta(seconds=start_sec)

        return data_dict

    data_dict = read_bdf_and_extract_data(bdf_file_path,start_sec,end_sec)
    if Filter:
        data_dict['Data'] = {ch: notch_filter(data, data_dict['SampleFrequency'],) for ch, data in data_dict['Data'].items()}
    else:
        data_dict['Data'] = {ch: np.squeeze(data) for ch, data in
                             data_dict['Data'].items()}
    # plt.plot(data_dict['Data']['Fpz'][0])
    # plt.show()

    # 获取bdf_file_path的父目录
    parent_dir = os.path.dirname(bdf_file_path)
    # 结合父目录和新文件夹名来创建完整的路径
    result_figures_path = os.path.join(parent_dir, "Result&Figures")
    # 检查该文件夹是否已存在，如果不存在则创建
    if not os.path.exists(result_figures_path):
        os.makedirs(result_figures_path)

    return data_dict,result_figures_path